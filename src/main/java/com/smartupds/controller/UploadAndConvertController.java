/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartupds.controller;

import com.smartupds.common.Resources;
import com.smartupds.impl.EnrichReferences;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.Objects;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author mafragias
 */
@Controller
public class UploadAndConvertController {

    @RequestMapping("/uploadfiles")
    public String uploadPage(Model model) {
        return "uploadview";
    }

    @RequestMapping("/upload")
    public String uploadAndConvert(Model model, @RequestParam("files") MultipartFile[] files,
            @RequestParam("sparqlquery") String sparqlQuery, @RequestParam("endpoint") String endpoint){
        
        String now = String.valueOf(new Date().getTime());
        new File(Resources.pathToDocsUploadsDirectory).mkdir();
        String uploadDocsDirectory = Resources.pathToDocsUploadsDirectory + now;
        String outputJATSDirectory;
        new File(uploadDocsDirectory).mkdir();
        new File(Resources.pathToJATSOutputsDirectory ).mkdir();
        new File(Resources.pathToFinalOutput).mkdir();

        File resultedXml;
        File fixedDOCTYPEIssueXml;
        EnrichReferences enrichReferences = new EnrichReferences();
        String fileExtension;

        for (MultipartFile file : files) {
            fileExtension = getFileExtension(Objects.requireNonNull(file.getOriginalFilename()));
            if(fileExtension.equals("docx") || fileExtension.equals("doc")){
                outputJATSDirectory = Resources.pathToJATSOutputsDirectory + file.getOriginalFilename() + now;
                Path fileNameAndPath = Paths.get(uploadDocsDirectory, file.getOriginalFilename());
                try {
                    Files.write(fileNameAndPath, file.getBytes());
                    System.out.println(
                            "------------------------------------------\n" +
                            file.getOriginalFilename() + " UPLOADED TO "+ Resources.pathToDocsUploadsDirectory + now +"\n"+
                            "------------------------------------------\n");
                    enrichReferences.runMeTypeset(fileExtension,uploadDocsDirectory + "/" + file.getOriginalFilename(),outputJATSDirectory + "/");
                    resultedXml = new File(outputJATSDirectory + "/nlm/out.xml");
                    fixedDOCTYPEIssueXml = enrichReferences.fixDOCTYPEIssueOfJATSCreation(resultedXml);
//                    System.out.println("TREXEI");
//                    System.out.println(fixedDOCTYPEIssueXml);
//                    System.out.println(endpoint);
//                    System.out.println(sparqlQuery);
                    enrichReferences.fixReferences(fixedDOCTYPEIssueXml, endpoint, sparqlQuery);
                    enrichReferences.updateDoc(uploadDocsDirectory + "/" + file.getOriginalFilename(), enrichReferences);
                } catch (IOException | InterruptedException e) {
                    e.printStackTrace();
                }
            }else{
                System.out.println("Please Choose docx or doc files");
            }
        }
        model.addAttribute("msg", "ok");
        return "uploadstatusview";
    }

    public String getFileExtension(String fileName){
        int i = fileName.lastIndexOf('.');
        if (i > 0) {
            return fileName.substring(i+1);
        }
        return "";
    }
}
