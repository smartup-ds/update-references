/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartupds.common;

import java.util.ArrayList;

/**
 *
 * @author mafragias
 */
public class Resources {
    public static String pathToDocsUploadsDirectory = System.getProperty("user.dir") + "/workspace/uploads/";
    public static String pathToJATSOutputsDirectory = System.getProperty("user.dir") + "/workspace/outputs/";
    public static String pathToFinalOutput = System.getProperty("user.dir") + "/workspace/output_final/";
    public static ArrayList<String> pathToRefListElementsOfXML = new ArrayList<String>() {
        {
            add("back");
            add("ref-list");
        }
    };
    public static String getListElementOfRefList = "mixed-citation";
    public static String pathToFixedDOCTYPEIssueXML = pathToJATSOutputsDirectory + "fixedNLM/";
    public static String fixedDOCTYPEIssueXMLFileName = "fixed_DOCTYPE.xml";
    public static final String finalXMLName = "final_xml.xml";
    
    public static final String METYPESET_PATH = "./workspace/MeTypeset/bin/meTypeset.py";
    
}
