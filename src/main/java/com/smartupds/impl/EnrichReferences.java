/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartupds.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import org.dom4j.Document;

import com.smartupds.common.Resources;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.query.GraphQuery;
import org.eclipse.rdf4j.query.GraphQueryResult;
import org.eclipse.rdf4j.query.Update;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.sparql.SPARQLRepository;

/**
 *
 * @author mafragias
 */
public class EnrichReferences {

    public HashSet<String> oldCitations;
    public HashSet<String> changedCitations;

    public EnrichReferences() {
        this.oldCitations = new HashSet<>();
        this.changedCitations = new HashSet<>();
    }

    public void fixReferences(File xmlFile, String endpoint, String sparqlQuery) {
        SAXReader reader = new SAXReader();
        reader.setEncoding("UTF-8");
        Document doc;
        try {
            doc = reader.read(new FileInputStream(xmlFile));
            
            Element refListElement = doc.getRootElement();
            for (String el : Resources.pathToRefListElementsOfXML) {
                if (refListElement != null) {
                    refListElement = refListElement.element(el);
                }
            }
            if (refListElement != null) {
                List<Element> refList = refListElement.elements();
                Repository repo = new SPARQLRepository(endpoint);
                repo.initialize();
                RepositoryConnection conn = repo.getConnection();
                String citation = "";
                String title = "";
                Element citationElementOut;
                String titleFromSparql = "";
                Boolean hasTitleFromSparql = false;
                String givenNameFromSparql = "";
                String familyNameFromSparql = "";
                String dateFromSparql = "";
                Boolean hasDateFromSparql = false;
                String urlFromSparql = "";
                Boolean hasUrlFromSparql = false;
                String doiFromSparql = "";
                Boolean hasDoiFromSparql = false;
                String spageFromSparql = "";
                Boolean hasSPageFromSparql = false;
                String lpageFromSparql = "";
                Boolean hasLPageFromSparql = false;
                for (int i = 0; i < refList.size(); i++) {
                    titleFromSparql = "";
                    hasTitleFromSparql = false;
                    dateFromSparql = "";
                    hasDateFromSparql = false;
                    urlFromSparql = "";
                    hasUrlFromSparql = false;
                    doiFromSparql = "";
                    hasDoiFromSparql = false;
                    spageFromSparql = "";
                    hasSPageFromSparql = false;
                    lpageFromSparql = "";
                    hasLPageFromSparql = false;
                    citationElementOut = refList.get(i).element(Resources.getListElementOfRefList);
                    String mySparqlQuery = "";
                    //IF THERE IS REFERENCE OF METYPESET TOOL
                    if (citationElementOut != null) {
                        citation = refList.get(i).elementText(Resources.getListElementOfRefList);
                        if (citation != null) {
                            Parser parser = new Parser(citation);
                            parser.parseTitle();
                            title = parser.getTitle();
                            mySparqlQuery=sparqlQuery.replace("?title", "\""+title+"\"");
//                        sparqlQuery = getSparqlQueryByCitationTitle(title);
                        }
                    }
                    GraphQuery ttlQuery = conn.prepareGraphQuery(mySparqlQuery);
                    GraphQueryResult ttlQueryResult = ttlQuery.evaluate();
                    ArrayList<String> authorsToAdd = new ArrayList<>();
                    Parser parser = new Parser(citation);
                    Statement ttlQueryElement;
                    String ttlQueryPredicate;
                    Value ttlQueryObject;
                    String ttlQuerySubject= "";
                    while (ttlQueryResult.hasNext()) {
                        ttlQueryElement = ttlQueryResult.next();
                        ttlQueryPredicate = ttlQueryElement.getPredicate().getLocalName();
                        ttlQueryObject = ttlQueryElement.getObject();
                        ttlQuerySubject = ttlQueryElement.getSubject().stringValue();

                        if (ttlQueryPredicate.equals("familyName")) {
                            familyNameFromSparql = ttlQueryObject.stringValue();
                        }
                        if (ttlQueryPredicate.equals("givenName")) {
                            givenNameFromSparql = ttlQueryObject.stringValue();
                            
                        }
                        if(!familyNameFromSparql.equals("") && !givenNameFromSparql.equals("")){
                            authorsToAdd.add(familyNameFromSparql + " " + givenNameFromSparql);
                            familyNameFromSparql = "";
                            givenNameFromSparql = "";
                        }
                        
                        if (ttlQueryPredicate.equals("title")) {
                            ttlQuerySubject = ttlQueryElement.getSubject().stringValue();
                            titleFromSparql = ttlQueryObject.stringValue();
                            oldCitations.add(titleFromSparql);
                            hasTitleFromSparql = true;
                        }
                        if (ttlQueryPredicate.equals("hasLiteralValue")) {
                            doiFromSparql = ttlQueryObject.stringValue();
                            hasDoiFromSparql = true;
                        }
                        if (ttlQueryPredicate.equals("publicationDate")) {
                            dateFromSparql = ttlQueryObject.stringValue();
                            hasDateFromSparql = true;
                        }
                        if (ttlQueryPredicate.equals("startingPage")) {
                            spageFromSparql = ttlQueryObject.stringValue();
                            hasSPageFromSparql = true;
                        }
                        if (ttlQueryPredicate.equals("endingPage")) {
                            lpageFromSparql = ttlQueryObject.stringValue();
                            hasLPageFromSparql = true;
                        }
                    }
                    for (String author : authorsToAdd) {
                        if (!citation.contains(author)) {
                            citation = parser.getCitationWithExtraAuthors(authorsToAdd);
                        }
                    }
                    if (!citation.contains(doiFromSparql)) {
                        citation = parser.insertString(citation, " doi: " + doiFromSparql, citation.length() - 1);
                    }
//                if(!contains)... //TODO CHECK HOW THE FORMAT WILL BE SO YOU CAN FILL THE REST.
                    refList.get(i).element(Resources.getListElementOfRefList).clearContent();
                    refList.get(i).element(Resources.getListElementOfRefList).addText(citation);
                    OutputFormat format = OutputFormat.createPrettyPrint();
                    XMLWriter xmlwriter = new XMLWriter(
                            new OutputStreamWriter(
                                    new FileOutputStream(Resources.pathToFinalOutput + Resources.finalXMLName), "UTF-8"
                            ), format
                    );
                    xmlwriter.write(doc);
                    xmlwriter.close();
                    this.changedCitations.add(citation);

                    if (!hasSPageFromSparql) {
                        //updateGraph(spageFromSparql, lpageFromSparql, ttlQuerySubject);
                    }

                }

            } else {
                System.out.println(
                        "------------------------------------------\n" +
                                "Wrong Path to XML ref-list.\n" +
                                "Check Resources.pathToRefListElementsOfXML\n" +
                                "------------------------------------------\n"
                );
            }

        } catch (DocumentException | IOException e) {
            e.printStackTrace();
        }
    }

    public void updateGraph(String spage, String lpage, String subjectToUpdate) {
        Repository repo = new SPARQLRepository("http://localhost:9999/blazegraph/#query");
        repo.initialize();
        RepositoryConnection conn = repo.getConnection();
        String sparqlQuery = "" +
                "PREFIX frbr: <http://purl.org/vocab/frbr/core#>\n" +
                "PREFIX prism: <http://prismstandard.org/namespaces/basic/2.0/>\n" +
                "INSERT DATA\n" +
                "{ <" + subjectToUpdate + "> frbr:embodiment    ?embodiment .\n" +
                "?embodiment prism:startingPage " + spage + " ;\n" +
                "prism:endingPage " + lpage + " .\n" +
                "}";
        Update update = conn.prepareUpdate(sparqlQuery);
        update.execute();
    }

    public void runMeTypeset(String argType, String argInput, String argOutput) throws InterruptedException {
        String pythonOSPath = "python";
        String pyScript = Resources.METYPESET_PATH;

        ProcessBuilder processBuilder = new ProcessBuilder(
                pythonOSPath,
                pyScript,
                argType,
                argInput,
                argOutput
        );
        processBuilder.redirectErrorStream(true);

        Process proc = null;
        try {
            proc = processBuilder.start();
        } catch (IOException e) {
            e.printStackTrace();
        }

        assert proc != null;
        BufferedReader stdInput = new BufferedReader(new
                InputStreamReader(proc.getInputStream()));

        BufferedReader stdError = new BufferedReader(new
                InputStreamReader(proc.getErrorStream()));

        // Read the output from the command
//        System.out.println("Here is the standard output of the command:\n");
        String s = null;
        while (true) {
            try {
                if ((s = stdInput.readLine()) == null) break;
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println(s);
        }

        // Read any errors from the attempted command
//        System.out.println("Here is the standard error of the command (if any):\n");
        while (true) {
            try {
                if ((s = stdError.readLine()) == null) break;
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println(s);
        }
        System.out.println("------------------------------------------\n" +
                " CONVERTED TO JATS TO "+Resources.pathToJATSOutputsDirectory+"/nlm/out.xml\n" +
                "------------------------------------------\n");
    }

    public File fixDOCTYPEIssueOfJATSCreation(File file) {

        List<String> lines = new ArrayList<>();
        try {
            new File(Resources.pathToFixedDOCTYPEIssueXML).mkdir();
            File newFile = new File(Resources.pathToFixedDOCTYPEIssueXML + Resources.fixedDOCTYPEIssueXMLFileName);
            FileWriter myWriter = new FileWriter(newFile);
            System.out.println(file.toPath());
//            lines = Files.readAllLines(file.toPath(), StandardCharsets.UTF_8);
            BufferedReader reader = new BufferedReader( new InputStreamReader(new FileInputStream(file.toPath().toString()), "UTF8"));
            String row = "";
            while(( row = reader.readLine())!=null ){
                lines.add(row);
            }
            reader.close();
            int i = 0;
            for (String line : lines) {
                if (i == 0) {
                    i++;
                    myWriter.write("<?xml version='1.0' encoding='UTF-8'?>\n");
                } else {
                    myWriter.write(line + "\n");
                }
            }
            myWriter.close();
            System.out.println( "------------------------------------------\n" +
                                " CONVERSION TO JATS FIXED DOCTYPE ISSUE\n" +
                                "------------------------------------------\n");
            return new File(Resources.pathToFixedDOCTYPEIssueXML + Resources.fixedDOCTYPEIssueXMLFileName);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void updateDoc(String originalDocPath, EnrichReferences enrichReferences) throws IOException {
        try {
            try {

                /**
                 * if uploaded doc then use HWPF else if uploaded Docx file use
                 * XWPFDocument
                 */
                XWPFDocument doc = new XWPFDocument(
                        OPCPackage.open(originalDocPath));
                for (XWPFParagraph p : doc.getParagraphs()) {
                    List<XWPFRun> runs = p.getRuns();
                    if (runs != null) {
                        for (XWPFRun r : runs) {
                            String text = r.getText(0);
                            for (String oldCitation : this.oldCitations) {
                                if (text != null && text.contains(oldCitation)) {
                                    for (String newCitation : this.changedCitations) {
                                        if (newCitation.contains(oldCitation)) {
                                            System.out.println(newCitation);
                                            r.setText(newCitation, 0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                doc.write(new FileOutputStream(Resources.pathToFinalOutput + "output.docx"));
                System.out.println("------------------------------------------\n" +
                        "THE END: NEW UPDATED DOCX FILE CREATED TO ROOT FOLDER\n" +
                        "------------------------------------------\n");
            } finally {
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}


//    public String getSparqlQueryByCitationTitle(String title) {
//        return "PREFIX cito: <http://purl.org/spar/cito/>\n" +
//                "PREFIX dcterms: <http://purl.org/dc/terms/>\n" +
//                "PREFIX datacite: <http://purl.org/spar/datacite/>\n" +
//                "PREFIX literal: <http://www.essepuntato.it/2010/06/literalreification/>\n" +
//                "PREFIX biro: <http://purl.org/spar/biro/>\n" +
//                "PREFIX frbr: <http://purl.org/vocab/frbr/core#>\n" +
//                "PREFIX c4o: <http://purl.org/spar/c4o/>\n" +
//                "PREFIX prism: <http://prismstandard.org/namespaces/basic/2.0/>\n" +
//                "PREFIX pro: <http://purl.org/spar/pro/>\n" +
//                "PREFIX foaf: <http://xmlns.com/foaf/0.1/>\n" +
//                "PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>\n" +
//                "\n" +
////                "SELECT ?paper ?title ?familyName ?givenName ?url ?doi ?spage ?lpage ?date " +
//                "CONSTRUCT {\n" +
//                "\t?paper dcterms:title ?title .\n" +
//                "    ?paper pro:isDocumentContextFor ?hasContributor .\n" +
//                "    ?hasContributor pro:withRole ?role ;\n" +
//                "      pro:isHeldBy ?author .\n" +
//                "    ?author foaf:familyName ?familyName;\n" +
//                "      foaf:givenName ?givenName .  \n" +
//                "    ?paper datacite:hasIdentifier [\n" +
//                "        datacite:usesIdentifierScheme datacite:doi ;\n" +
//                "        literal:hasLiteralValue ?doi\n" +
//                "      ].\n" +
//                "    ?paper frbr:embodiment ?embodiment .\n" +
//                "    ?embodiment prism:startingPage ?spage ;\n" +
//                "      prism:endingPage ?lpage .\n" +
//                "\n" +
//                "    ?paper prism:publicationDate ?date .\n" +
//                "}\n" +
//                "WHERE {\n" +
//                "?paper dcterms:title \"" + title + "\" ;\n" +
//                "\t\tdcterms:title ?title .\n" +
//                "  OPTIONAL {\n" +
//                "    ?paper pro:isDocumentContextFor ?hasContributor .\n" +
//                "    ?hasContributor pro:withRole ?role ;\n" +
//                "    \t\t\t\tpro:isHeldBy ?author .\n" +
//                "    ?author foaf:familyName ?familyName;\n" +
//                "        \tfoaf:givenName ?givenName .  \n" +
//                "  }\n" +
////                "  OPTIONAL {\n" +
////                "    ?paper datacite:hasIdentifier [\n" +
////                "           datacite:usesIdentifierScheme datacite:url ;\n" +
////                "           literal:hasLiteralValue ?url\n" +
////                "    ]\n" +
////                "  }\n" +
//                "  OPTIONAL {\n" +
//                "    ?paper datacite:hasIdentifier [\n" +
//                "           datacite:usesIdentifierScheme datacite:doi ;\n" +
//                "           literal:hasLiteralValue ?doi\n" +
//                "    ]\n" +
//                "  }\n" +
//                "  OPTIONAL {\n" +
//                "    ?paper frbr:embodiment ?embodiment .\n" +
//                "    ?embodiment prism:startingPage ?spage ;\n" +
//                "    \t\t\tprism:endingPage ?lpage .\n" +
//                "  }\n" +
//                "  OPTIONAL {\n" +
//                "  \t?paper prism:publicationDate ?date .\n" +
//                "  }\n" +
//                "}";
//    }
//
//    TupleQuery tupleQuery;
//    TupleQueryResult tqr;
//                    tupleQuery = conn.prepareTupleQuery(QueryLanguage.SPARQL, refDataSparqlQuery);

//    String citationSparql = "";
//    StringBuilder authors;
//    ArrayList<String> authorsXML;
//    ArrayList<String> authorsEndpoint;
//    String lName = "";
//    Element titleElement;
//    String doi = "";
//    Element doiElement;
//    List<Element> pg;
//    Element citationElementOutGood;
//                authors = new StringBuilder();
//                citationElementOutGood = refList.get(i).element("element-citation");
//                        //IF THERE IS REFERENCE OF TYPESET TOOL
//                        if (citationElementOutGood != null) {
//                        //IF REFERENCE HAS DOI
//                        doiElement = citationElementOutGood.element("object-id");
//                        if (doiElement != null) {
//                        doi = citationElementOutGood.element("object-id").getStringValue();
//                        //DOIS ARE UNIQUE, SO WE CAN SEARCH FROM DOI
//                        refDataSparqlQuery = "SELECT ?cited WHERE {\n" +
//                        "OPTIONAL { \n" +
//                        "\t\t?cited dcterms:title \"" + title + "\"\n" +
//                        "\t}\n" +
//                        "}";
//                        }
//                        titleElement = citationElementOutGood.element("article-title");
//                        //IF REFERENCE HAS TITLE
//                        if (titleElement != null) {
//                        title = titleElement.getStringValue();
//                        //TITLES ARE UNIQUE, SO WE CAN SEARCH FROM TITLE
//                        refDataSparqlQuery = "SELECT ?cited WHERE {\n" +
//                        "OPTIONAL { \n" +
//                        "\t\t?cited dcterms:title \"" + title + "\"\n" +
//                        "\t}\n" +
//                        "}";
//                        }
//                        //IF REFERENCE HAS AUTHORS
//                        if (citationElementOutGood.element("person-group") != null) {
//                        pg = citationElementOutGood.element("person-group").elements("name");
//                        for (int j = 0; j < pg.size(); j++) {
//        fName = pg.get(j).element("given-names").getStringValue();
//        lName = pg.get(j).element("surname").getStringValue();
//        authors.append(lName + " " + fName + ", ");
//        }
//        }
//        }


//                try {
//                    tqr = tupleQuery.evaluate();
//                    ArrayList<String> authorsToAdd = new ArrayList<>();
//                    ArrayList<String> authorsExists = new ArrayList<>();
//                    while (tqr.hasNext()) {
//                        try {
//                            BindingSet tupleQueryElement = tqr.next();
//                            if (tupleQueryElement.getValue("title") != null)
//                                titleFromSparql = tupleQueryElement.getValue("title").toString().replace("\"", "").split("\\^")[0];
//                            if (tupleQueryElement.getValue("familyName") != null)
//                                familyNameFromSparql = tupleQueryElement.getValue("familyName").toString().replace("\"", "").split("\\^")[0];
//                            if (tupleQueryElement.getValue("givenName") != null)
//                                givenNameFromSparql = tupleQueryElement.getValue("givenName").toString().replace("\"", "").split("\\^")[0];
////                            if(tupleQueryElement.getValue("url")!=null)
////                                urlFromSparql = tqr.next().getValue("url").toString().replace("\"", "").split("\\^")[0];
//                            if (tupleQueryElement.getValue("doi") != null)
//                                doiFromSparql = tupleQueryElement.getValue("doi").toString().replace("\"", "").split("\\^")[0];
//                            if (tupleQueryElement.getValue("spage") != null)
//                                spageFromSparql = tupleQueryElement.getValue("spage").toString().replace("\"", "").split("\\^")[0];
//                            if (tupleQueryElement.getValue("lpage") != null)
//                                lpageFromSparql = tupleQueryElement.getValue("lpage").toString().replace("\"", "").split("\\^")[0];
//                            if (tupleQueryElement.getValue("date") != null)
//                                dateFromSparql = tupleQueryElement.getValue("date").toString().replace("\"", "").split("\\^")[0];
//                            assert citation != null;
//
//                            //System.out.println("1 " + citation);
//                            if(tqr.hasNext()){
//                                if (!citation.contains(familyNameFromSparql)) {
//                                    hasAuthor = false;
//                                    authorsToAdd.add(familyNameFromSparql + " " + givenNameFromSparql);
//                                }else{
//                                    authorsExists.add(familyNameFromSparql + " " + givenNameFromSparql);
//                                }
//                            }else{
//
//
//
//
//
//
//
//
//                                Parser parser = new Parser(citation);
//                                if(!hasAuthor){
//                                    citation = parser.getCitationWithExtraAuthors(authorsToAdd);
//                                    //System.out.println("2 " + citation);
//                                }else{
//
//                                }
//                                if (citation.contains(titleFromSparql)) {
//                                    this.oldCitations.add(titleFromSparql);
//                                    hasTitle = true;
//                                } else {
//                                    hasTitle = false;
//                                }
//                                if (citation.contains(doiFromSparql)) {
//                                    hasDoi = true;
//                                } else {
//                                    hasDoi = false;
//                                    citation = parser.insertString(citation, " doi: " + doiFromSparql, citation.length()-1);
//                                    //System.out.println("3 " + citation);
//                                }
//                                if (citation.contains(spageFromSparql)) {
//                                    hasSpage = true;
//                                } else {
//                                    hasSpage = false;
//                                }
//                                if (citation.contains(lpageFromSparql)) {
//                                    hasLpage = true;
//                                } else {
//                                    hasLpage = false;
//                                }
//                                if (citation.contains(dateFromSparql)) {
//                                    hasDate = true;
//                                } else {
//                                    hasDate = false;
//                                }
//
//                                refList.get(i).element(Resources.getListElementOfRefList).clearContent();
//                                refList.get(i).element(Resources.getListElementOfRefList).addText(citation);
//                                OutputFormat format = OutputFormat.createPrettyPrint();
//                                XMLWriter xmlwriter = new XMLWriter(
//                                        new OutputStreamWriter(
//                                                new FileOutputStream(xmlFile.getPath().replace(".xml", "") + Resources.updatedXMLName), "UTF-8"
//                                        ), format
//                                );
//                                Utils.copyFile(xmlFile, new File(Resources.pathToUpdatedXML + Resources.finalXMLName));
//                                xmlwriter.write(doc);
//                                xmlwriter.close();
//                                this.changedCitations.add(citation);
//                            }
//
//
//                        } catch (Exception e) {
//                            //TODO: KEEP TITLES OF PAPERS THAT THROW ERROR
//                            e.printStackTrace();
//                        }
//                    }
//                } catch (Exception e) {
//                    //TODO: KEEP TITLES OF PAPERS THAT THROW ERROR
//                    e.printStackTrace();
//                }
