/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartupds.impl;

import java.util.ArrayList;

/**
 *
 * @author mafragias
 */
public class Parser {
    private String text;
    private ArrayList<String> authors;
    private String title;

    public Parser(String text) {
        this.text = text;
        this.authors = new ArrayList<>();
    }

    public void parseTitle() {
        int indexOfFirstDot = text.indexOf(".");
        int indexOfSecondDot = text.indexOf(".", indexOfFirstDot + 1);
        title = text.substring(indexOfFirstDot + 2, indexOfSecondDot);
    }

    public String getTitle() {
        return title;
    }

    public String getCitationWithExtraAuthors(ArrayList<String> missingAuthors) {
        int indexOfFirstDot = text.indexOf(".");
        if (text.contains("et al")) {
            text = text.replace(" et al", ", ");
            indexOfFirstDot = text.indexOf(".");
        }
        StringBuilder authors = new StringBuilder();
        int i = 0;
        for (String missingAuthor : missingAuthors) {
            if (i == missingAuthors.size() - 1) {
                authors.append(missingAuthor);
            } else {
                authors.append(missingAuthor + ", ");
            }
            i++;
        }
        return insertString(text, authors.toString(), indexOfFirstDot - 1);
    }


    public String insertString(
            String originalString,
            String stringToBeInserted,
            int index) {

        // Create a new string
        String newString = new String();

        for (int i = 0; i < originalString.length(); i++) {

            // Insert the original string character
            // into the new string
            newString += originalString.charAt(i);

            if (i == index) {

                // Insert the string to be inserted
                // into the new string
                newString += stringToBeInserted;
            }
        }

        //System.out.println(newString);
        // return the modified String
        return newString;
    }
}

