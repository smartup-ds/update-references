# Clone Repository & Build and Run in Netbeans #

### Clone, Build, & Run ###

* Clone :
```
git clone https://mafragias@bitbucket.org/smartup-ds/update-references.git
```
* Build :
```
cd update-references
mvn clean && mvn package
```
or 

Open project from Netbeans and build it.

* Run :

Run from Netbeans to start the process.


### Open on Browser ###

* localhost:8080/uploadfiles

### INPUTS ###

* **Document** : input_document.docx
* **Query** : input_query.txt
* **Endpoint** : https://opencitations.net/sparql
